package auth

import (
	"crypto/rsa"
	"time"
)

// Authenticator auth types which must have token method
type Authenticator interface {
	GenerateToken(u UserClaims) (string, error)
}

// Auth implements authenticator interface
type Auth struct {
	PrivateKey    *rsa.PrivateKey
	TokenDuration time.Duration
	Subject       string
}

// GenerateToken generates jwt token from claims
func (a *Auth) GenerateToken(u UserClaims) (string, error) {
	c := a.NewBikeClaims(u)
	return GenerateJWTToken(c, a.PrivateKey)
}
