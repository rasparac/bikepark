package errors

const (
	CreateUserErrMsg = "An error occured while attempting to create a BikePark account. Please try again."
	EditUserErrMsg   = "An error occured while attempting to edit a BikePark account. Please try again."
	GetUserErrMsg    = "An error occured while attempting to get a BikePark account. Please try again."
	DeleteUserErrMsg = "An error occured while attempting to delete a BikePark account. Please try again."

	UpdateUserLocationErrMsg = "An error occured while attempting to update your address. Please try again."
)
