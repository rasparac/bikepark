package errors

const (
	CreateBikeTheftErrMsg = "An error occured while attempting to create a BikePark theft. Please try again."
	EditBikeTheftErrMsg   = "An error occured while attempting to edit a BikePark theft. Please try again."
	GetBikeTheftErrMsg    = "An error occured while attempting to get a BikePark theft. Please try again."
	DeleteBikeTheftErrMsg = "An error occured while attempting to delete a BikePark theft. Please try again."
)
