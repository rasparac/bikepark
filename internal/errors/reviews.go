package errors

const (
	CreateReviewErrMsg = "An error occured while attempting to create a Review. Please try again."
	EditReviewErrMsg   = "An error occured while attempting to edit a Review. Please try again."
	GetReviewErrMsg    = "An error occured while attempting to get a Review. Please try again."
	DeleteReviewErrMsg = "An error occured while attempting to delete a Review. Please try again."
	MyReviewsErrMsg    = "An error occured while attempting to get your Reviews. Please try again."
)
