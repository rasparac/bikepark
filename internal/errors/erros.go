package errors

import "fmt"

type Error struct {
	err error
	msg string
}

// NewError returns new database error
func NewError(err error, msg string) *Error {
	return &Error{err, msg}
}

func (dr *Error) Error() string {
	return dr.err.Error()
}

// Is match error in error chain
func (dr *Error) Is(err error) bool {
	_, ok := err.(*Error)
	return ok
}

// NotFoundErr holds info for not found error
type NotFoundErr struct {
	Resource string
	Inner    error
}

// NewNotFoundErr returns new not found error
func NewNotFoundErr(resource string, inner error) *NotFoundErr {
	return &NotFoundErr{resource, inner}
}

func (nfe *NotFoundErr) Error() string {
	return fmt.Sprintf("%s with not found", nfe.Resource)
}

// Is match error in error chain
func (nfe *NotFoundErr) Is(err error) bool {
	_, ok := err.(*NotFoundErr)
	return ok
}

type InternalServerErr struct {
	msg   string
	inner error
}

// NewInteralServerError returns internal server error
func NewInteralServerError(msg string, inner error) error {
	return &InternalServerErr{
		msg:   msg,
		inner: inner,
	}
}

func (ise *InternalServerErr) Error() string {
	if ise.inner != nil {
		return fmt.Sprintf("%s: %s", ise.msg, ise.inner)
	}
	return ise.msg
}

// Is match error in error chain
func (nfe *InternalServerErr) Is(err error) bool {
	_, ok := err.(*InternalServerErr)
	return ok
}

type PermissionDeniedErr struct {
	msg   string
	inner error
}

// NewInteralServerError returns internal server error
func NewPermissionDeniedErr(msg string, inner error) error {
	return &PermissionDeniedErr{
		msg:   msg,
		inner: inner,
	}
}

func (pde *PermissionDeniedErr) Error() string {
	if pde.inner != nil {
		return fmt.Sprintf("%s: %s", pde.msg, pde.inner)
	}
	return pde.msg
}

// Is match error in error chain
func (pde *PermissionDeniedErr) Is(err error) bool {
	_, ok := err.(*PermissionDeniedErr)
	return ok
}
