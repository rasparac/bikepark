package errors

const (
	CreateBikeParkErrMsg = "An error occured while attempting to create a BikePark location. Please try again."
	EditBikeParkErrMsg   = "An error occured while attempting to edit a BikePark location. Please try again."
	GetBikeParkErrMsg    = "An error occured while attempting to get a BikePark location. Please try again."
	DeleteBikeParkErrMsg = "An error occured while attempting to delete a BikePark location. Please try again."
	MyBikeParkErrMsg     = "An error occured while attempting to get your BikePark locations. Please try again."
)
