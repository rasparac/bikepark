package postgres

import "fmt"

// Error contains database error information
type Error struct {
	err error
	msg string
}

// NewError returns new database error
func NewError(err error, msg string) *Error {
	return &Error{err, msg}
}

func (dr *Error) Error() string {
	return dr.err.Error()
}

// Is match error in error chain
func (dr *Error) Is(err error) bool {
	_, ok := err.(*Error)
	return ok
}

// NotFoundErr holds info for not found error
type NotFoundErr struct {
	Resource string
	Inner    error
}

// NewNotFoundErr returns new not found error
func NewNotFoundErr(resource string, inner error) *NotFoundErr {
	return &NotFoundErr{resource, inner}
}

func (nfe *NotFoundErr) Error() string {
	return fmt.Sprintf("%s not found", nfe.Resource)
}

// Is match error in error chain
func (nfe *NotFoundErr) Is(err error) bool {
	_, ok := err.(*NotFoundErr)
	return ok
}

// UniqueViolationErr holds info for unique database errors
type UniqueViolationErr struct {
	Key   string
	Msg   string
	inner error
}

func (uve *UniqueViolationErr) Error() string {
	return uve.inner.Error()
}

// NewUniqueViolationErr returns new unique violation error
func NewUniqueViolationErr(key, msg string, inner error) *UniqueViolationErr {
	return &UniqueViolationErr{
		Key:   key,
		Msg:   msg,
		inner: inner,
	}
}

// Is match error in error chain
func (uve *UniqueViolationErr) Is(err error) bool {
	_, ok := err.(*UniqueViolationErr)
	return ok
}
