package postgres

import (
	"context"
	"errors"

	"github.com/google/uuid"
	"github.com/jackc/pgerrcode"
	"github.com/jackc/pgx/v4"
	"gitlab.com/thebikepark/bikepark/internal/models"
	"golang.org/x/crypto/bcrypt"
)

type UsersManager struct {
	db *PsqlDatabase
}

func NewUsersManager(db *PsqlDatabase) *UsersManager {
	return &UsersManager{
		db: db,
	}
}

// CreateUser runs create user sql query
func (um *UsersManager) Create(ctx context.Context, u models.User) (models.User, error) {
	password, err := hashPassword([]byte(u.Password))
	if err != nil {
		return u, err
	}

	var (
		out         = u
		insertQuery = `
		INSERT INTO users(username, full_name, password)
		VALUES ($1, $2, $3)
		RETURNING
			id,
			created_at,
			updated_at`
	)

	err = um.db.db.QueryRow(ctx, insertQuery, u.Username, u.FullName, string(password)).Scan(&out.ID, &out.CreatedAt, &out.UpdatedAt)
	if err != nil {
		if pgerrcode.IsIntegrityConstraintViolation(Code(err)) {
			return u, NewUniqueViolationErr("username", "Username already taken", err)
		}
		return u, NewError(err, "failed to insert new user into database")
	}

	out.IsActive = true

	return out, nil
}

// EditUser runs update user sql query
func (um *UsersManager) Update(ctx context.Context, u models.User) (models.User, error) {
	var (
		out         = u
		updateQuery = `
		UPDATE users
		SET
			username = $1, full_name = $2
		WHERE
			id = $3
		RETURNING
			username, full_name, is_active, created_at, updated_at`
	)

	tx, err := um.db.db.Begin(ctx)
	if err != nil {
		return u, NewError(err, "failed to start transaction")
	}
	defer func() {
		_ = tx.Rollback(ctx)
	}()

	err = um.db.db.QueryRow(ctx, updateQuery, u.Username, u.FullName, u.ID).
		Scan(
			&out.Username,
			&out.FullName,
			&out.IsActive,
			&out.CreatedAt,
			&out.UpdatedAt,
		)
	if err != nil {
		if pgerrcode.IsIntegrityConstraintViolation(Code(err)) {
			return u, NewUniqueViolationErr("username", "Username alredy taken", err)
		}
		return out, NewError(err, "failed to insert new user into database")
	}

	return out, nil
}

func (um *UsersManager) Delete(ctx context.Context, ID uuid.UUID) error {

	deleteQuery := `
		UPDATE users
		SET
			username = encode(sha256(username::bytea), 'hex'),
			full_name = encode(sha256(full_name::bytea), 'hex'),
			is_active = false,
			deleted_at = NOW()
		WHERE id = $1`

	_, err := um.db.db.Exec(ctx, deleteQuery, ID)
	if err != nil {
		return NewError(err, "failed to delete user")
	}

	return nil
}

// UpdateLocation updates user location in database by user id
func (um *UsersManager) UpdateLocation(ctx context.Context, ID uuid.UUID, ul models.UserLocation) (models.UserLocation, error) {
	var (
		out         = ul
		updateQuery = `
			INSERT INTO users_location(user_id, address, latitude, longitude)
			VALUES($1, $2, $3, $4)
			ON CONFLICT (user_id)
			DO UPDATE
			SET
				address = EXCLUDED.address,
				latitude = EXCLUDED.latitude,
				longitude = EXCLUDED.longitude
		`
	)

	_, err := um.db.db.Exec(ctx, updateQuery, ID, ul.Address, ul.Latitude, ul.Longitude)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return out, NewNotFoundErr("user_location", err)
		}
		return out, NewError(err, "failed to update user location")
	}
	return out, nil
}

// GetUser runs select query and returns single user by id
func (um *UsersManager) Get(ctx context.Context, ID uuid.UUID) (models.User, error) {
	var (
		u           models.User
		selectQuery = `
		SELECT
			u.id, u.username, u.full_name, u.is_active, u.created_at, u.updated_at,
			ul.address, ul.latitude, ul.longitude
		FROM
			users u
		LEFT JOIN
			users_location ul ON ul.user_id = u.id
		WHERE
			u.id = $1
		`
	)
	err := um.db.db.QueryRow(ctx, selectQuery, ID).Scan(
		&u.ID,
		&u.Username,
		&u.FullName,
		&u.IsActive,
		&u.CreatedAt,
		&u.UpdatedAt,
		&u.UserLocation.Address,
		&u.UserLocation.Latitude,
		&u.UserLocation.Longitude,
	)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return u, NewNotFoundErr("user", err)
		}
		return u, NewError(err, "failed to create select user sql query")
	}

	return u, nil
}

func (um *UsersManager) List(ctx context.Context, f models.UsersFilter) ([]models.User, error) {
	selectQuery := `
		SELECT
			u.id, u.username, u.full_name, u.is_active, u.created_at, u.updated_at, u.expires_at, u.deleted_at,
			ul.address, ul.latitude, ul.longitude
		FROM
			users u
		LEFT JOIN
			users_location ul ON ul.user_id = u.id
		ORDER BY
			u.created_at DESC
		`

	rows, err := um.db.db.Query(ctx, selectQuery)
	if err != nil {
		return nil, NewError(err, "failed to create select user sql query")
	}
	defer rows.Close()

	var users []models.User

	for rows.Next() {
		var u models.User
		err = rows.Scan(
			&u.ID,
			&u.Username,
			&u.FullName,
			&u.IsActive,
			&u.CreatedAt,
			&u.UpdatedAt,
			&u.ExpiresAt,
			&u.DeletedAt,
			&u.UserLocation.Address,
			&u.UserLocation.Latitude,
			&u.UserLocation.Longitude,
		)
		if err != nil {
			return nil, NewError(err, "failed to scan user struct")
		}
		users = append(users, u)
	}

	return users, nil
}

// hashPassword takse password and return generated hash from password
func hashPassword(password []byte) ([]byte, error) {
	hash, err := bcrypt.GenerateFromPassword(password, bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}
	return hash, nil
}
