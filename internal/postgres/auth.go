package postgres

import (
	"context"

	"gitlab.com/thebikepark/bikepark/internal/models"
)

// AuthDatabase contract which provides database methods to get auth user
type AuthDatabase interface {
	GetUserByUsername(ctx context.Context, username string) (*models.AuthUser, error)
}

// GetUserByUsername returns user by username
func (pq PsqlDatabase) GetUserByUsername(ctx context.Context, username string) (*models.AuthUser, error) {

	selectQuery := `
		SELECT
			u.username, u.id, u.password
		FROM users u
		WHERE
			u.username = $1
	`
	row := pq.db.QueryRow(ctx, selectQuery, username)

	var u models.AuthUser
	err := row.Scan(
		&u.Username,
		&u.ID,
		&u.Password,
	)
	if err != nil {
		return nil, NewError(err, "failed to scan auth user")
	}

	return &u, nil
}
