package postgres

import (
	"context"
	"fmt"
	"strings"

	"github.com/google/uuid"
	"gitlab.com/thebikepark/bikepark/internal/models"
)

type (
	ReviewsManager struct {
		db *PsqlDatabase
	}

	ReviewsFilter struct {
		PlaceID uuid.UUID
		UserID  uuid.UUID
	}
)

func NewReviewsManager(db *PsqlDatabase) *ReviewsManager {
	return &ReviewsManager{
		db: db,
	}
}

func (rm *ReviewsManager) Upsert(ctx context.Context, in models.Review) (models.Review, error) {
	var (
		out = in
		q   = `
			INSERT INTO
				reviews (user_id, place_id, comment, rating)
			VALUES ($1, $2, $3, $4)
			ON CONFLICT
				(user_id, place_id)
			DO UPDATE
			SET
				comment	= EXCLUDED.comment,
				rating	= EXCLUDED.rating
			RETURNING id, created_at, updated_at
		`
	)

	return out, rm.db.db.QueryRow(ctx, q, in.UserID, in.PlaceID, in.Comment, in.Rating).Scan(&out.ID, &out.CreatedAt, &out.UpdatedAt)
}

func (rm *ReviewsManager) List(ctx context.Context, filter models.ReviewsFilter) ([]models.Review, error) {
	var (
		reviews       = []models.Review{}
		whereClauses  []string
		args          []interface{}
		queryTemplate = `
			SELECT
				r.id, r.user_id, r.place_id, r.comment, r.rating, r.created_at, r.updated_at,
				u.username
			FROM reviews r
			LEFT JOIN users u
				ON u.id = r.user_id
			WHERE %s
		`
	)

	if filter.PlaceID != uuid.Nil {
		args = append(args, filter.PlaceID)
		whereClauses = append(whereClauses, fmt.Sprintf("r.place_id = $%d", len(args)))
	}

	if filter.UserID != uuid.Nil {
		args = append(args, filter.UserID)
		whereClauses = append(whereClauses, fmt.Sprintf("r.user_id = $%d", len(args)))
	}

	query := fmt.Sprintf(queryTemplate, strings.Join(whereClauses, " AND "))

	rows, err := rm.db.db.Query(
		ctx,
		query,
		args...,
	)
	if err != nil {
		return reviews, err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			review   models.Review
			username string
		)
		err := rows.Scan(
			&review.ID,
			&review.UserID,
			&review.PlaceID,
			&review.Comment,
			&review.Rating,
			&review.CreatedAt,
			&review.UpdatedAt,
			&username,
		)
		if err != nil {
			return reviews, err
		}
		review.User.Username = username
		reviews = append(reviews, review)
	}

	return reviews, rows.Err()
}

func (rm *ReviewsManager) Delete(ctx context.Context, ID uuid.UUID, userID uuid.UUID) error {
	result, err := rm.db.db.Exec(ctx, "DELETE FROM reviews WHERE id = $1 AND user_id = $2", ID, userID)
	if err != nil {
		return err
	}

	if result.RowsAffected() == 0 {
		return NewNotFoundErr("reviews", nil)
	}

	return nil
}
