package postgres

import (
	"context"
	"fmt"
	"strings"

	"github.com/google/uuid"
	"gitlab.com/thebikepark/bikepark/internal/models"
)

type (
	// BikeParkManager handles database logic for bike parkings
	BikeParkManager struct {
		db *PsqlDatabase
		um *UsersManager
	}
)

func NewBikeParkManager(db *PsqlDatabase, um *UsersManager) *BikeParkManager {
	return &BikeParkManager{db: db, um: um}
}

// Create executes sql query to create location in database
func (bpm *BikeParkManager) Create(ctx context.Context, bp models.BikePark) (models.BikePark, error) {
	var (
		out         = bp
		insertQuery = `
		INSERT INTO bike_parkings(user_id, street, city, is_suggestion, latitude, longitude, description)
		VALUES($1, $2, $3, $4, $5, $6, $7)
		RETURNING id, created_at, updated_at`
	)

	err := bpm.db.db.QueryRow(ctx, insertQuery, bp.UserID, bp.Street, bp.City, bp.IsSuggestion, bp.Latitude, bp.Longitude, bp.Description).
		Scan(
			&out.ID,
			&out.CreatedAt,
			&out.UpdatedAt,
		)
	if err != nil {
		return out, NewError(err, "failed to insert new bike park into database")
	}

	u, err := bpm.um.Get(ctx, bp.UserID)
	if err != nil {
		return out, NewError(err, "failed to get user from database")
	}

	out.User = u

	return out, nil
}

// Update executes sql query to update location in database
func (bpm *BikeParkManager) Update(ctx context.Context, bp models.BikePark) (models.BikePark, error) {
	var (
		out         = bp
		updateQuery = `
		UPDATE bike_parkings
		SET 
			street = $1,
			city = $2,
			is_suggestion = $3,
			latitude = $4,
			longitude = $5,
			description = $6
		WHERE id = $7 AND user_id = $8
		RETURNING created_at, updated_at`
	)

	err := bpm.db.db.QueryRow(ctx, updateQuery, bp.Street, bp.City, bp.IsSuggestion, bp.Latitude, bp.Longitude, bp.Description, bp.ID, bp.UserID).
		Scan(
			&out.CreatedAt,
			&out.UpdatedAt,
		)
	if err != nil {
		return out, NewError(err, "failed to update bike park")
	}

	u, err := bpm.um.Get(ctx, bp.UserID)
	if err != nil {
		return out, NewError(err, "failed to get user from database")
	}

	out.User = u

	return out, nil
}

// List executes sql query to get latest locations from database
func (bpm *BikeParkManager) List(ctx context.Context, f models.BikeparkFilter) ([]models.BikePark, error) {
	var (
		whereClause = []string{}
		args        []interface{}
		posCounter  = 1
		selectQuery = `
	SELECT
		bp.id, bp.user_id, bp.street, bp.city, bp.is_suggestion, bp.description, bp.latitude, bp.longitude, bp.created_at, bp.updated_at,
		u.id, u.username, u.full_name, u.is_active, u.created_at, u.updated_at
	FROM
		bike_parkings bp
	JOIN users u
		ON
			u.id = bp.user_id
	%s
	ORDER BY
		bp.created_at DESC
	`
	)

	if f.City != "" {
		whereClause = append(whereClause, fmt.Sprintf("bp.city LIKE $%d", posCounter))
		args = append(args, f.City)
		posCounter++
	}

	if f.Street != "" {
		whereClause = append(whereClause, fmt.Sprintf("bp.street LIKE $%d", posCounter))
		args = append(args, f.Street)
		posCounter++
	}

	if f.UserID != uuid.Nil {
		whereClause = append(whereClause, fmt.Sprintf("bp.user_id = $%d", posCounter))
		args = append(args, f.UserID)
	}

	if f.IsDeleted != nil && !*f.IsDeleted {
		whereClause = append(whereClause, "bp.deleted_at IS NULL")
	} else {
		whereClause = append(whereClause, "bp.deleted_at IS NOT NULL")
	}

	conditions := strings.Join(whereClause, " AND ")
	if conditions != "" {
		conditions = "WHERE " + conditions
	}

	selectQuery = fmt.Sprintf(selectQuery, conditions)

	rows, err := bpm.db.db.Query(ctx, selectQuery, args...)
	if err != nil {
		return nil, NewError(err, "failed to list bike parkings")
	}
	defer rows.Close()

	var bps []models.BikePark
	for rows.Next() {
		var l models.BikePark
		var u models.User
		if err := rows.Scan(
			&l.ID,
			&l.UserID,
			&l.Street,
			&l.City,
			&l.IsSuggestion,
			&l.Description,
			&l.Latitude,
			&l.Longitude,
			&l.CreatedAt,
			&l.UpdatedAt,
			&u.ID,
			&u.Username,
			&u.FullName,
			&u.IsActive,
			&u.CreatedAt,
			&u.UpdatedAt,
		); err != nil {
			return nil, err
		}
		l.User = u
		bps = append(bps, l)
	}

	return bps, nil
}

// Delete executes delete sql and deletes location from database
func (bpm *BikeParkManager) Delete(ctx context.Context, UUID, userID uuid.UUID) error {
	_, err := bpm.db.db.Exec(ctx, "UPDATE bike_parkings SET deleted_at = NOW() WHERE id = $1 AND user_id = $2", UUID, userID)
	if err != nil {
		return NewError(err, "failed to execute delete location sql query")
	}

	return nil
}

// NearestLocations query nearest location based on filter
// select *
// 	from (
// 		select *,
// 		(
// 			acos(
// 				sin(radians(45.807434)) *
// 				sin(radians(latitude)) +
// 				cos(radians(45.807434)) *
// 				cos(radians(latitude)) *
// 				cos(radians(16.02378674499556 - longitude))
// 			) * 6371
// 		) as distance from locations
// 	)
// locations where distance < 10 where order by distance;
func (bpm *BikeParkManager) Nearest(ctx context.Context, f models.NearestLocationFilter, l int) ([]models.BikePark, error) {
	q := `SELECT
			bike_parkings.id,
			bike_parkings.user_id,
			bike_parkings.street,
			bike_parkings.city,
			bike_parkings.is_suggestion,
			bike_parkings.description,
			bike_parkings.latitude,
			bike_parkings.longitude,
			bike_parkings.created_at,
			bike_parkings.updated_at,
			users.id,
			users.username,
			users.full_name,
			users.is_active,
			users.created_at,
			users.updated_at
			FROM 
			(
				SELECT *,
				(
					ACOS(
						SIN(RADIANS($1)) *
						SIN(RADIANS(latitude)) +
						COS(RADIANS($2)) *
						COS(RADIANS(latitude)) *
						COS(RADIANS($3 - longitude))
					) * 6371
				) AS distance FROM bike_parkings WHERE deleted_at IS NULL
			) bike_parkings
			JOIN users ON users.id = bike_parkings.user_id
			WHERE distance < $4 ORDER BY distance LIMIT $5;`

	rows, err := bpm.db.db.Query(ctx, q, f.Latitude, f.Latitude, f.Longitude, f.Distance, l)
	if err != nil {
		return nil, NewError(err, "get nearest locations")
	}
	defer rows.Close()

	var bps []models.BikePark
	for rows.Next() {
		var bp models.BikePark
		var u models.User
		if err := rows.Scan(
			&bp.ID,
			&bp.UserID,
			&bp.Street,
			&bp.City,
			&bp.IsSuggestion,
			&bp.Description,
			&bp.Latitude,
			&bp.Longitude,
			&bp.CreatedAt,
			&bp.UpdatedAt,
			&u.ID,
			&u.Username,
			&u.FullName,
			&u.IsActive,
			&u.CreatedAt,
			&u.UpdatedAt,
		); err != nil {
			return nil, err
		}
		bp.User = u
		bps = append(bps, bp)
	}

	return bps, nil
}
