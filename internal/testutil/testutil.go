package testutil

import (
	"context"

	"gitlab.com/thebikepark/bikepark/pkg/auth"
	"google.golang.org/grpc/metadata"
)

// CtxWithMetadata returns valid metadata for testing
func CtxWithMetadata() context.Context {
	ctx := context.Background()
	ctx = metadata.NewIncomingContext(ctx, metadata.MD{
		auth.UsernameHeaderKey: []string{"test"},
		auth.UUIDHeaderKey:     []string{"users/uuid"},
	})
	return ctx
}

// CtxWithoutUUID returns invalid metadata for testing. Withoud name header.
func CtxWithoutUUID() context.Context {
	ctx := context.Background()
	ctx = metadata.NewIncomingContext(ctx, metadata.MD{
		auth.UsernameHeaderKey: []string{"test"},
	})
	return ctx
}
