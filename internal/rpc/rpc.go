package rpc

import (
	"context"
	"net"
	"os"
	"os/signal"

	"github.com/pkg/errors"
	bikepark "gitlab.com/thebikepark/bikepark/pkg/grpc/v1"
	"go.uber.org/zap"
	"google.golang.org/grpc"
)

// StartServer will start grpc server for user management
func StartServer(ctx context.Context, log *zap.Logger, s bikepark.BikeParkServer, a bikepark.BikeParkAuthServer, port string) error {
	sock, err := net.Listen("tcp", preparePort(port))
	if err != nil {
		return errors.Wrap(err, "failed to bind sock listener")
	}

	grpcServer := grpc.NewServer()
	bikepark.RegisterBikeParkServer(grpcServer, s)
	bikepark.RegisterBikeParkAuthServer(grpcServer, a)

	// graceful shutdown
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		for range c {
			// sig is a ^C, handle it
			log.Info("shutting down gRPC server...")

			grpcServer.GracefulStop()

			<-ctx.Done()
		}
	}()

	return grpcServer.Serve(sock)
}

func preparePort(port string) string {
	if string(port[0]) != ":" {
		return ":" + port
	}

	return port
}
