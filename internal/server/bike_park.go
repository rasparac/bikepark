package server

import (
	"context"
	"errors"

	"gitlab.com/thebikepark/bikepark/internal/app"
	"gitlab.com/thebikepark/bikepark/internal/business/bikeparkings"
	bpErrors "gitlab.com/thebikepark/bikepark/internal/errors"
	"gitlab.com/thebikepark/bikepark/internal/models"
	bikepark_data_v1 "gitlab.com/thebikepark/bikepark/pkg/grpc/v1"
	"go.uber.org/zap"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

// CreateBikeParkings creates location
func (s *Service) CreateBikeParking(ctx context.Context, req *bikepark_data_v1.CreateBikeParkingRequest) (*bikepark_data_v1.BikeParking, error) {
	log := s.log.With(zap.String("handler", "bikepark/server.Service.CreateLocation"))

	newBp, err := bikeparkings.Create(ctx, models.APIParkingToData(req.GetBikeParking()), s.bpm)
	if err != nil {
		log.Error("failed to create bike parking", zap.Error(err))
		return nil, bpErrors.NewAPIStatusErr(err, bpErrors.CreateBikeParkErrMsg).Err()
	}

	l, err := models.DataParkingToAPI(newBp)
	if err != nil {
		log.Error("failed to parse bike parking to API", zap.Error(err))
		return nil, bpErrors.InternalServerError().Err()
	}

	log.Debug("successfully finished create bike parking request")

	return l, nil
}

// EditBikeParkings updates location
func (s *Service) EditBikeParking(ctx context.Context, req *bikepark_data_v1.EditBikeParkingRequest) (*bikepark_data_v1.BikeParking, error) {
	log := s.log.With(zap.String("handler", "bikepark/server.Service.EditBikeParking"))

	bikeParkUUID, err := app.ExtractIDFromResourceName(req.GetName(), 2)
	if err != nil {
		log.Error("failed to extract ID from resource", zap.Error(err))
		return nil, bpErrors.NewAPIStatusErr(err, bpErrors.EditBikeParkErrMsg).Err()
	}

	loc := models.APIParkingToData(req.GetBikeParking())
	loc.ID = bikeParkUUID

	updatedBp, err := bikeparkings.Update(ctx, loc, s.bpm)
	if err != nil {
		log.Error("failed to edit bike parking", zap.Error(err))
		return nil, bpErrors.NewAPIStatusErr(err, bpErrors.EditBikeParkErrMsg).Err()
	}

	l, err := models.DataParkingToAPI(updatedBp)
	if err != nil {
		log.Error("failed to parse bike parking to API", zap.Error(err))
		return nil, bpErrors.InternalServerError().Err()
	}

	log.Debug("successfully finished edit bike parking request")

	return l, nil
}

// ListBikeParkings returns list of locations
func (s *Service) ListBikeParkings(ctx context.Context, req *bikepark_data_v1.ListBikeParkingsRequest) (*bikepark_data_v1.ListBikeParkingsResponse, error) {
	log := s.log.With(zap.String("handler", "bikepark/server.Service.ListBikeParkings"))

	locs, err := bikeparkings.List(ctx, s.bpm)
	if err != nil {
		log.Error("failed to get locations", zap.Error(err))
		return nil, bpErrors.NewAPIStatusErr(err, bpErrors.GetBikeParkErrMsg).Err()
	}

	apiLocs, err := models.DataParkingsToAPI(locs)
	if err != nil {
		log.Error("failed to parse bike parking to API", zap.Error(err))
		return nil, bpErrors.InternalServerError().Err()
	}

	log.Debug("successfully finished list bike parkings request")

	return &bikepark_data_v1.ListBikeParkingsResponse{
		BikeParkings: apiLocs,
	}, nil
}

// DeleteBikeParkings deletes location
func (s *Service) DeleteBikeParking(ctx context.Context, req *bikepark_data_v1.DeleteBikeParkingRequest) (*emptypb.Empty, error) {
	log := s.log.With(zap.String("handler", "bikepark/server.Service.DeleteBikeParking"))

	UUID, err := app.ExtractIDFromResourceName(req.GetName(), 2)
	if err != nil {
		log.Error("failed to extract ID from resource", zap.Error(err))
		return nil, bpErrors.NewAPIStatusErr(err, bpErrors.DeleteBikeParkErrMsg).Err()
	}

	if err := bikeparkings.Delete(ctx, UUID, s.bpm); err != nil {
		log.Error("failed to delete bike parking", zap.Error(err))
		return nil, bpErrors.NewAPIStatusErr(err, bpErrors.DeleteBikeParkErrMsg).Err()
	}

	log.Debug("successfully finished list bike parkings request")

	return &emptypb.Empty{}, nil
}

// NearestBikeParkings returns a list of nearest locations based on latitude and longitude
// Latitude : max/min +90 to -90
// Longitude : max/min +180 to -180
func (s *Service) NearestBikeParkings(ctx context.Context, in *bikepark_data_v1.NearestBikeParkingsRequest) (*bikepark_data_v1.ListBikeParkingsResponse, error) {
	log := s.log.With(zap.String("handler", "bikepark/server.Service.NearestBikeParkings"))

	limit := 50
	if in.Limit > 0 {
		limit = int(in.Limit)
	}

	if in.Filter == nil {
		log.Error("missing filter")
		return nil, status.New(codes.InvalidArgument, "Filter is required").Err()
	}

	if in.Filter.Latitude > 90 || in.Filter.Latitude < -90 {
		invalidLat := bpErrors.Details("latitude", "Invalid latitude range. Must be between 90 and -90")
		err := bpErrors.NewValidationErr(errors.New("invalid latitude"), invalidLat)
		log.Error("invalid latitude", zap.Error(err))
		return nil, bpErrors.NewAPIStatusErr(err, "Invalid Arguments").Err()
	}

	if in.Filter.Longitude > 180 || in.Filter.Longitude < -180 {
		invalidLng := bpErrors.Details("longitude", "Invalid longitude range. Must be between 180 and -180")
		err := bpErrors.NewValidationErr(errors.New("invalid longitude"), invalidLng)
		log.Error("invalid longitude", zap.Error(err))
		return nil, bpErrors.NewAPIStatusErr(err, "Invalid Arguments").Err()
	}

	if in.Filter.Distance <= 0 {
		in.Filter.Distance = 2
	}

	f := models.NearestLocationFilter{
		Distance:  in.GetFilter().GetDistance(),
		Latitude:  in.GetFilter().GetLatitude(),
		Longitude: in.Filter.GetLongitude(),
	}

	dbLocs, err := bikeparkings.Nearest(ctx, f, limit, s.bpm)
	if err != nil {
		log.Error("failed to get nearest bike parkings", zap.Error(err))
		return nil, bpErrors.NewAPIStatusErr(err, "An error occurred.").Err()
	}

	locs, err := models.DataParkingsToAPI(dbLocs)
	if err != nil {
		log.Error("failed to parse bike parking to API", zap.Error(err))
		return nil, bpErrors.InternalServerError().Err()
	}

	log.Debug("successfully finished list nearest bike parkings request")

	return &bikepark_data_v1.ListBikeParkingsResponse{
		BikeParkings: locs,
	}, nil
}

// MyBikeParkings returns locations created by logged in user
func (s *Service) MyBikeParkings(ctx context.Context, in *emptypb.Empty) (*bikepark_data_v1.ListBikeParkingsResponse, error) {
	log := s.log.With(zap.String("handler", "bikepark/server.Service.MyBikeParkings"))

	dbLocs, err := bikeparkings.My(ctx, s.bpm)
	if err != nil {
		log.Error("failed to fetch bike parkings", zap.Error(err))
		return nil, bpErrors.NewAPIStatusErr(err, bpErrors.MyBikeParkErrMsg).Err()
	}

	locs, err := models.DataParkingsToAPI(dbLocs)
	if err != nil {
		log.Error("failed to parse bike parking to API", zap.Error(err))
		return nil, bpErrors.InternalServerError().Err()
	}

	log.Debug("successfully finished list my bike parkings request")

	return &bikepark_data_v1.ListBikeParkingsResponse{
		BikeParkings: locs,
	}, nil
}
