package server

import (
	"errors"

	bpErrors "gitlab.com/thebikepark/bikepark/internal/errors"
	bikepark_data_v1 "gitlab.com/thebikepark/bikepark/pkg/grpc/v1"
)

func validateNearestTheftFilter(f *bikepark_data_v1.NearestBikeTheftsFilter) error {
	if f.Latitude > 90 || f.Latitude < -90 {
		invalidLat := bpErrors.Details("latitude", "Invalid latitude range. Must be between 90 and -90")
		return bpErrors.NewValidationErr(errors.New("invalid latitude"), invalidLat)
	}
	if f.Longitude > 180 || f.Longitude < -180 {
		invalidLng := bpErrors.Details("longitude", "Invalid longitude range. Must be between 180 and -180")
		return bpErrors.NewValidationErr(errors.New("invalid longitude"), invalidLng)
	}
	return nil
}
