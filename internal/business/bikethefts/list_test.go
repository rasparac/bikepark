package bikethefts

import (
	"context"
	"errors"
	"testing"

	"github.com/c2fo/testify/require"
	"github.com/google/uuid"
	"gitlab.com/thebikepark/bikepark/internal/models"
	bikepark_data_v1 "gitlab.com/thebikepark/bikepark/pkg/grpc/v1"
)

type mockNearestBikeThefts struct {
	shouldErr               bool
	nearestBikeShouldReturn []models.BikeTheft
}

func (mnbt *mockNearestBikeThefts) Nearest(ctx context.Context, f models.NearestBikeTheftsFilter, limit int32) ([]models.BikeTheft, error) {
	if mnbt.shouldErr {
		return nil, errors.New("get bike thefts error")
	}
	return mnbt.nearestBikeShouldReturn, nil
}

func (mnbt *mockNearestBikeThefts) List(ctx context.Context, f models.BikeTheftsFilter) ([]models.BikeTheft, error) {
	if mnbt.shouldErr {
		return nil, errors.New("get bike thefts error")
	}
	return nil, nil
}

func TestNearestBikeThefts(t *testing.T) {

	id1 := uuid.New()
	id2 := uuid.New()

	userId1 := uuid.New()
	userId2 := uuid.New()

	mocked := []models.BikeTheft{
		{
			ID:       id1,
			UserID:   userId1,
			City:     "World City",
			Latitude: 1,
		},
		{
			ID:       id2,
			UserID:   userId2,
			City:     "Europe City",
			Latitude: 2,
		},
	}

	type args struct {
		ctx   context.Context
		r     Reader
		f     *bikepark_data_v1.NearestBikeTheftsFilter
		limit int32
	}
	tests := []struct {
		name    string
		args    args
		want    []models.BikeTheft
		wantErr bool
		errMsg  string
	}{
		{
			name: "it should return the list of filtered bike thefts",
			args: args{
				ctx: context.Background(),
				r: &mockNearestBikeThefts{
					nearestBikeShouldReturn: []models.BikeTheft{mocked[0]},
				},
				f: &bikepark_data_v1.NearestBikeTheftsFilter{
					Latitude: 1,
				},
				limit: 1,
			},
			want: []models.BikeTheft{
				{
					ID:       id1,
					UserID:   userId1,
					City:     "World City",
					Latitude: 1,
				},
			},
		},
		{
			name: "it should return an error if get bike theft fails",
			args: args{
				ctx: context.Background(),
				r: &mockNearestBikeThefts{
					shouldErr: true,
				},
			},
			wantErr: true,
			errMsg:  "get bike thefts error",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Nearest(tt.args.ctx, tt.args.r, tt.args.f, tt.args.limit)
			if tt.wantErr {
				require.EqualError(t, err, tt.errMsg)
				return
			}
			require.NoError(t, err)
			require.Equal(t, tt.want, got)
		})
	}
}
