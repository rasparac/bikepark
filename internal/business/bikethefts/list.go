package bikethefts

import (
	"context"

	"gitlab.com/thebikepark/bikepark/internal/app"
	bpErrors "gitlab.com/thebikepark/bikepark/internal/errors"
	"gitlab.com/thebikepark/bikepark/internal/models"
	"gitlab.com/thebikepark/bikepark/pkg/bikepark"
	v1 "gitlab.com/thebikepark/bikepark/pkg/grpc/v1"
)

// Reader bike theft reader
type Reader interface {
	Nearest(ctx context.Context, f models.NearestBikeTheftsFilter, limit int32) ([]models.BikeTheft, error)
	List(context.Context, models.BikeTheftsFilter) ([]models.BikeTheft, error)
}

// NearestBikeThefts returns list of nearest bike thefts
func Nearest(ctx context.Context, r Reader, f *v1.NearestBikeTheftsFilter, limit int32) ([]models.BikeTheft, error) {
	dbFilter := models.NearestBikeTheftsFilter{
		Distance:  f.GetDistance(),
		Latitude:  f.GetLatitude(),
		Longitude: f.GetLongitude(),
	}
	return r.Nearest(ctx, dbFilter, limit)
}

func List(ctx context.Context, r Reader) ([]models.BikeTheft, error) {
	return r.List(ctx, models.BikeTheftsFilter{
		IsDeleted: bikepark.Bool(false),
	})
}

func My(ctx context.Context, r Reader) ([]models.BikeTheft, error) {
	request, err := app.RequesterFromCtx(ctx)
	if err != nil {
		return nil, bpErrors.NewInteralServerError("failed to get requester from context", err)
	}

	return r.List(ctx, models.BikeTheftsFilter{UserID: request.UserID, IsDeleted: bikepark.Bool(false)})
}
