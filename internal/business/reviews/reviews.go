package reviews

import (
	"context"

	"github.com/google/uuid"
	"gitlab.com/thebikepark/bikepark/internal/models"
	v1 "gitlab.com/thebikepark/bikepark/pkg/grpc/v1"
)

type (
	writer interface {
		Upsert(ctx context.Context, review models.Review) (models.Review, error)
		Delete(ctx context.Context, ID, UserID uuid.UUID) error
	}

	reader interface {
		List(ctx context.Context, f models.ReviewsFilter) ([]models.Review, error)
	}
)

func Create(ctx context.Context, review *v1.Review, w writer) (models.Review, error) {
	dbReview, err := models.APIReviewToData(review)
	if err != nil {
		return models.Review{}, err
	}

	return w.Upsert(ctx, dbReview)
}

func List(ctx context.Context, f *v1.ReviewsFilter, r reader) ([]models.Review, error) {
	var placeID uuid.UUID
	if f.GetPlaceId() != "" {
		var err error
		placeID, err = uuid.Parse(f.GetPlaceId())
		if err != nil {
			return nil, err
		}
	}

	var userID uuid.UUID
	if f.GetUserId() != "" {
		var err error
		userID, err = uuid.Parse(f.GetUserId())
		if err != nil {
			return nil, err
		}
	}

	return r.List(ctx, models.ReviewsFilter{
		PlaceID: placeID,
		UserID:  userID,
	})
}

func Delete(ctx context.Context, reviewID, userID uuid.UUID, w writer) error {
	return w.Delete(ctx, reviewID, userID)
}
