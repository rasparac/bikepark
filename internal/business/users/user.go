package users

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"gitlab.com/thebikepark/bikepark/internal/app"
	bpErrors "gitlab.com/thebikepark/bikepark/internal/errors"
	"gitlab.com/thebikepark/bikepark/internal/models"
	bikepark_data_v1 "gitlab.com/thebikepark/bikepark/pkg/grpc/v1"
)

type Writer interface {
	Create(ctx context.Context, u models.User) (models.User, error)
	Update(ctx context.Context, bp models.User) (models.User, error)
	UpdateLocation(ctx context.Context, ID uuid.UUID, ul models.UserLocation) (models.UserLocation, error)
	Delete(ctx context.Context, id uuid.UUID) error
}

type Reader interface {
	List(ctx context.Context, f models.UsersFilter) ([]models.User, error)
	Get(ctx context.Context, id uuid.UUID) (models.User, error)
}

func Create(ctx context.Context, u *bikepark_data_v1.User, w Writer) (models.User, error) {
	user := models.User{
		Username: u.GetUsername(),
		Password: u.GetPassword(),
		FullName: sql.NullString{
			String: u.GetFullName(),
			Valid:  u.GetFullName() != "",
		},
	}
	return w.Create(ctx, user)
}

func Update(ctx context.Context, u models.User, w Writer) (models.User, error) {
	return w.Update(ctx, u)
}

func Delete(ctx context.Context, id uuid.UUID, w Writer) error {
	return w.Delete(ctx, id)
}

func UpdatLocation(ctx context.Context, id uuid.UUID, ul *bikepark_data_v1.UserLocation, w Writer) (models.UserLocation, error) {
	return w.UpdateLocation(ctx, id, models.APIUserLocationToData(ul))
}

func List(ctx context.Context, r Reader) ([]models.User, error) {
	return r.List(ctx, models.UsersFilter{})
}

func Get(ctx context.Context, name string, r Reader) (models.User, error) {
	id, err := app.ExtractIDFromResourceName(name, 2)
	if err != nil {
		return models.User{}, bpErrors.NewInteralServerError("failed to extract id from name", err)
	}

	return r.Get(ctx, id)
}
