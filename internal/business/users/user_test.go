package users

import (
	"context"
	"errors"
	"reflect"
	"testing"

	"github.com/google/uuid"
	"gitlab.com/thebikepark/bikepark/internal/models"
)

type UserManagerMock struct {
	shouldCreateRetunErr  bool
	shouldUpdateRetrunErr bool
	shouldDeletReturnErr  bool
	shouldListReturnErr   bool
	shouldGetRetrunErr    bool

	returnCreateUser models.User
	returnUpdateUser models.User
	returnListUsers  []models.User
	returnGetUser    models.User
}

func (umm *UserManagerMock) Create(ctx context.Context, u models.User) (models.User, error) {
	if umm.shouldCreateRetunErr {
		return umm.returnCreateUser, errors.New("error")
	}
	return umm.returnCreateUser, nil
}

func (umm *UserManagerMock) Update(ctx context.Context, bp models.User) (models.User, error) {
	if umm.shouldUpdateRetrunErr {
		return umm.returnUpdateUser, errors.New("error")
	}
	return umm.returnUpdateUser, nil
}

func (umm *UserManagerMock) Delete(ctx context.Context, id uuid.UUID) error {
	if umm.shouldDeletReturnErr {
		return errors.New("error")
	}
	return nil
}

func (umm *UserManagerMock) List(ctx context.Context, f models.UsersFilter) ([]models.User, error) {
	if umm.shouldListReturnErr {
		return umm.returnListUsers, errors.New("error")
	}
	return umm.returnListUsers, nil
}

func (umm *UserManagerMock) Get(ctx context.Context, id uuid.UUID) (models.User, error) {
	if umm.shouldGetRetrunErr {
		return umm.returnGetUser, errors.New("error")
	}
	return umm.returnGetUser, nil
}

func TestUpdate(t *testing.T) {
	type args struct {
		ctx context.Context
		u   models.User
		w   Writer
	}
	tests := []struct {
		name    string
		args    args
		want    models.User
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Update(tt.args.ctx, tt.args.u, tt.args.w)
			if (err != nil) != tt.wantErr {
				t.Errorf("Update() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Update() = %v, want %v", got, tt.want)
			}
		})
	}
}
