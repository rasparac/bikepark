package bikeparkings

import (
	"context"

	"github.com/google/uuid"
	"gitlab.com/thebikepark/bikepark/internal/app"
	bpErrors "gitlab.com/thebikepark/bikepark/internal/errors"
	"gitlab.com/thebikepark/bikepark/internal/models"
	"gitlab.com/thebikepark/bikepark/pkg/bikepark"
)

const MyErrMsg = "An error occured while attempting to get your locations. Please try again."

type Writer interface {
	Create(ctx context.Context, bp models.BikePark) (models.BikePark, error)
	Update(ctx context.Context, bp models.BikePark) (models.BikePark, error)
	Delete(ctx context.Context, UUID, UserID uuid.UUID) error
}

type Reader interface {
	List(context.Context, models.BikeparkFilter) ([]models.BikePark, error)
	Nearest(ctx context.Context, f models.NearestLocationFilter, l int) ([]models.BikePark, error)
}

func Create(ctx context.Context, bp models.BikePark, w Writer) (models.BikePark, error) {
	r, err := app.RequesterFromCtx(ctx)
	if err != nil {
		return bp, bpErrors.NewInteralServerError("failed to get requester from ctx", err)
	}

	bp.UserID = r.UserID

	return w.Create(ctx, bp)
}

func Update(ctx context.Context, bp models.BikePark, w Writer) (models.BikePark, error) {
	r, err := app.RequesterFromCtx(ctx)
	if err != nil {
		return bp, bpErrors.NewInteralServerError("failed to get requester from ctx", err)
	}

	bp.UserID = r.UserID

	return w.Update(ctx, bp)
}

func Delete(ctx context.Context, UUID uuid.UUID, w Writer) error {
	r, err := app.RequesterFromCtx(ctx)
	if err != nil {
		return bpErrors.NewInteralServerError("failed to get requester from ctx", err)
	}

	return w.Delete(ctx, UUID, r.UserID)
}

func List(ctx context.Context, r Reader) ([]models.BikePark, error) {
	return r.List(ctx, models.BikeparkFilter{
		IsDeleted: bikepark.Bool(false),
	})
}

func Nearest(ctx context.Context, f models.NearestLocationFilter, limit int, r Reader) ([]models.BikePark, error) {
	return r.Nearest(ctx, f, limit)
}

func My(ctx context.Context, r Reader) ([]models.BikePark, error) {
	request, err := app.RequesterFromCtx(ctx)
	if err != nil {
		return nil, bpErrors.NewInteralServerError(MyErrMsg, err)
	}

	return r.List(ctx, models.BikeparkFilter{
		UserID:    request.UserID,
		IsDeleted: bikepark.Bool(false),
	})
}
