package app

import (
	"errors"
	"fmt"
	"strings"

	"github.com/google/uuid"
)

// UserResourceName returns resource name based on user database id
func UserResourceName(id uuid.UUID) string {
	return fmt.Sprintf("users/%s", id.String())
}

// BikeParkResourceName returns resource name based on location database id
func BikeParkResourceName(id uuid.UUID) string {
	return fmt.Sprintf("bikeparkings/%s", id.String())
}

// BikeTheftResourceName returns resource name base on biketheft database id
func BikeTheftResourceName(id uuid.UUID) string {
	return fmt.Sprintf("bikethefts/%s", id.String())
}

// ReviewResourceName returns resource name for the API
func ReviewResourceName(id uuid.UUID) string {
	return fmt.Sprintf("reviews/%s", id.String())
}

// ExtractIDFromResourceName extracts id from resource name based on
// provided position. It assumes format like `bookshelf/12/book/34`
// starts with 1
func ExtractIDFromResourceName(s string, position int) (uuid.UUID, error) {
	if s == "" {
		return uuid.Nil, errors.New("please provide resource")
	}

	split := strings.Split(s, "/")
	if position > len(split) {
		return uuid.Nil, errors.New("position is bigger than resource positions")
	}

	return uuid.Parse(split[position-1])
}
