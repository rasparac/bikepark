CREATE EXTENSION citext;

CREATE TABLE IF NOT EXISTS users (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    username CITEXT NOT NULL,
    full_name TEXT DEFAULT NULL,
    password TEXT NOT NULL,
    is_active BOOLEAN NOT NULL DEFAULT '1',
    created_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP,
    expires_at TIMESTAMPTZ DEFAULT NULL,
    deleted_at TIMESTAMPTZ DEFAULT NULL
);

CREATE INDEX users_deleted_at_idx ON users (deleted_at);
CREATE INDEX users_is_active_idx ON users (is_active);
CREATE INDEX users_updated_at_idx ON users (updated_at);
CREATE UNIQUE INDEX users_username_idx ON users(username);

CREATE TABLE IF NOT EXISTS users_location (
    user_id UUID PRIMARY KEY,
    address TEXT DEFAULT NULL,
    city TEXT DEFAULT NULL,
    latitude NUMERIC(20, 18) DEFAULT NULL,
    longitude NUMERIC(20, 17) DEFAULT NULL,
    created_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT fk_users
        FOREIGN KEY (user_id)
        REFERENCES users (id)
);

CREATE INDEX users_location_updated_at_idx ON users_location (updated_at);
CREATE INDEX users_location_address_idx ON users_location (address);
CREATE INDEX users_location_city_idx ON users_location (city);
CREATE INDEX users_location_longitude_idx ON users_location (longitude);
CREATE INDEX users_location_latitude_idx ON users_location (latitude);

CREATE TABLE IF NOT EXISTS bike_parkings (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    user_id UUID NOT NULL,
    street TEXT,
    city TEXT,
    is_suggestion BOOLEAN NOT NULL DEFAULT '0',
    description TEXT,
    latitude NUMERIC(20, 18) NOT NULL,
    longitude NUMERIC(20, 17) NOT NULL,
    created_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP,
    deleted_at TIMESTAMPTZ DEFAULT NULL,

    CONSTRAINT fk_users
        FOREIGN KEY (user_id)
        REFERENCES users (id)
);

CREATE INDEX bike_parkings_user_id_idx ON bike_parkings (user_id);
CREATE INDEX bike_parkings_longitude_idx ON bike_parkings (longitude);
CREATE INDEX bike_parkings_city_idx ON bike_parkings (city);
CREATE INDEX bike_parkings_latitude_idx ON bike_parkings (latitude);
CREATE INDEX bike_parkings_updated_at_idx ON bike_parkings (updated_at);

CREATE TABLE IF NOT EXISTS bike_thefts(
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    user_id UUID NOT NULL,
    street TEXT,
    city TEXT,
    description TEXT,
    is_found BOOLEAN NOT NULL DEFAULT '0',
    latitude NUMERIC(20, 18) NOT NULL,
    longitude NUMERIC(20, 17) NOT NULL,
    created_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP,
    deleted_at TIMESTAMPTZ DEFAULT NULL,

    CONSTRAINT fk_users
        FOREIGN KEY (user_id)
        REFERENCES users (id)
);

CREATE INDEX bike_thefts_user_id_idx ON bike_thefts (user_id);
CREATE INDEX bike_thefts_longitude_idx ON bike_thefts (longitude);
CREATE INDEX bike_thefts_city_idx ON bike_thefts (city);
CREATE INDEX bike_thefts_latitude_idx ON bike_thefts (latitude);
CREATE INDEX bike_thefts_is_found_idx ON bike_thefts (is_found);
CREATE INDEX bike_thefts_updated_at_idx ON bike_thefts (updated_at);

CREATE TABLE IF NOT EXISTS bike_shops(
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    user_id UUID NOT NULL,
    street TEXT,
    city TEXT,
    description TEXT,
    latitude NUMERIC(20, 18) NOT NULL,
    longitude NUMERIC(20, 17) NOT NULL,
    created_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP,
    deleted_at TIMESTAMPTZ DEFAULT NULL,

    CONSTRAINT fk_users
        FOREIGN KEY (user_id)
        REFERENCES users (id)
);

CREATE INDEX bike_shops_user_id_idx ON bike_shops (user_id);
CREATE INDEX bike_shops_longitude_idx ON bike_shops (longitude);
CREATE INDEX bike_shops_city_idx ON bike_shops (city);
CREATE INDEX bike_shops_latitude_idx ON bike_shops (latitude);
CREATE INDEX bike_shops_updated_at_idx ON bike_shops (updated_at);


CREATE TABLE IF NOT EXISTS reviews(
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    user_id UUID NOT NULL,
    place_id UUID NOT NULL,
    comment TEXT,
    rating INT NOT NULL,

    created_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT fk_users
        FOREIGN KEY (user_id)
        REFERENCES users (id)
);

CREATE UNIQUE INDEX IF NOT EXISTS review_user_place_id_idx ON reviews(user_id, place_id);
CREATE INDEX IF NOT EXISTS review_rating_idx ON reviews(rating);

CREATE FUNCTION update_updated_at_column() RETURNS trigger
    LANGUAGE plpgsql
AS
$$
BEGIN
    NEW.updated_at = now();
    RETURN NEW;
END;
$$;

CREATE TRIGGER users_modified
    BEFORE UPDATE
    ON users
    FOR EACH ROW
EXECUTE FUNCTION update_updated_at_column();

CREATE TRIGGER bike_parkings_modified
    BEFORE UPDATE
    ON bike_parkings
    FOR EACH ROW
EXECUTE FUNCTION update_updated_at_column();

CREATE TRIGGER bike_thefts_modified
    BEFORE UPDATE
    ON bike_thefts
    FOR EACH ROW
EXECUTE FUNCTION update_updated_at_column();

CREATE TRIGGER bike_shops_modified
    BEFORE UPDATE
    ON bike_shops
    FOR EACH ROW
EXECUTE FUNCTION update_updated_at_column();

CREATE TRIGGER reviews_modified
    BEFORE UPDATE
    ON reviews
    FOR EACH ROW
EXECUTE FUNCTION update_updated_at_column();

CREATE TRIGGER users_location_modified
    BEFORE UPDATE
    ON users_location
    FOR EACH ROW
EXECUTE FUNCTION update_updated_at_column();