DROP INDEX IF EXISTS bt_user_uuid_idx;
DROP INDEX IF EXISTS bike_theft_uuid_idx;
DROP INDEX IF EXISTS bp_user_uuid_idx;
DROP INDEX IF EXISTS bike_park_uuid_idx;
DROP INDEX IF EXISTS bs_user_uuid_idx;
DROP INDEX IF EXISTS bike_shop_uuid_idx;


DROP TABLE bike_parkings;
DROP TABLE bike_thefts;
DROP TABLE bike_shops;
DROP TABLE users;
DROP TABLE reviews;