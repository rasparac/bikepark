package main

import (
	"context"
	"log"
	"time"

	_ "github.com/doug-martin/goqu/v9/dialect/postgres"
	_ "github.com/lib/pq"
	"github.com/namsral/flag"
	"gitlab.com/thebikepark/bikepark/internal/postgres"
	"gitlab.com/thebikepark/bikepark/internal/rpc"
	"gitlab.com/thebikepark/bikepark/internal/server"
	"gitlab.com/thebikepark/bikepark/pkg/auth"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
)

type config struct {
	server struct {
		port  string
		isTLS bool
		env   string
	}
	auth struct {
		jwtSigningKey string
		subject       string
		jwtTokenLife  time.Duration
	}
	database struct {
		user     string
		host     string
		name     string
		port     string
		password string
	}
}

func main() {
	c := config{}
	flag.StringVar(&c.server.port, "grpc_port", ":50001", "grpc port")
	flag.BoolVar(&c.server.isTLS, "grpc_tls", false, "grpc connection use tls")

	flag.StringVar(&c.database.user, "db_user", "bikepark", "database user")
	flag.StringVar(&c.database.host, "db_host", "127.0.0.1", "database host")
	flag.StringVar(&c.database.port, "db_port", "5432", "database port")
	flag.StringVar(&c.database.name, "db_name", "bikepark", "database name")
	flag.StringVar(&c.database.password, "db_password", "bikepark", "database password")

	flag.StringVar(&c.auth.jwtSigningKey, "jwt_signing_key", "/etc/pki/tls/CA/certs/auth-key.pem", "key for signing the JWTs")
	flag.StringVar(&c.auth.subject, "jwt_subject", "bikepark", "jwt subject")
	flag.DurationVar(&c.auth.jwtTokenLife, "jwt_token_lifetime", 24*time.Hour, "the duration of time that the token should live")

	flag.StringVar(&c.server.env, "env", "production", "environment")

	flag.Parse()

	var logger *zap.Logger
	if c.server.env == "production" {
		w := zapcore.AddSync(&lumberjack.Logger{
			Filename:   "./log/file.log",
			MaxSize:    500, // megabytes
			MaxBackups: 3,
			MaxAge:     28, // days
		})
		core := zapcore.NewCore(
			zapcore.NewJSONEncoder(zap.NewProductionEncoderConfig()),
			w,
			zap.DebugLevel,
		)
		logger = zap.New(core)
	} else {
		var err error
		logger, err = zap.NewDevelopment()
		if err != nil {
			panic(err)
		}
	}

	defer func() {
		err := logger.Sync()
		if err != nil {
			log.Println(err.Error())
		}
	}()
	dbConf := postgres.DBConfig{
		Host: c.database.host,
		Name: c.database.name,
		Pass: c.database.password,
		Port: c.database.port,
		User: c.database.user,
	}

	db, err := postgres.NewPsqlConn(dbConf)
	if err != nil {
		logger.Fatal("failed to create databse connection", zap.Error(err))
	} else {
		logger.Info("connected to database")
	}

	defer func() {
		logger.Info("closing database", zap.String("event", "db.close"))
		db.Close()
	}()

	if err := db.Ping(); err != nil {
		logger.Fatal("no db connection")
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	logger.Info("starting bikepark gprc server")

	key, err := auth.ReadPrivateKey(c.auth.jwtSigningKey)
	if err != nil {
		logger.Warn("failed to read private auth key", zap.Error(err))
	}

	auth := auth.Auth{
		PrivateKey:    key,
		Subject:       c.auth.subject,
		TokenDuration: c.auth.jwtTokenLife,
	}
	a := server.NewAuthService(logger, db, db, &auth)

	s, err := server.New(ctx, logger, db)
	if err != nil {
		logger.Fatal("failed to start server", zap.Error(err))
	}

	err = rpc.StartServer(ctx, logger, s, a, c.server.port)
	if err != nil {
		logger.Error("shuting down bikepark server", zap.Error(err))
	}
}
