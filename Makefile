MAIN_PATH=cmd/bikepark/main.go
VERSION:=1.0.0
PROTOSRC=./api
PROTO_PATH=$(PROTOSRC)/bikepark/*.proto
MIGRATIONS_PATH=./assets/migrations
GOARCH:=amd64
GOOS:=linux
DOCKERNET:=bikepark
PRODUCT:=bikepark
BUILDFLAGS:=""
BUILD:=$(shell date +%s)
GOENVS:=CGO_ENABLED=0
# db args
DB_PASS=bikepark
DB_USER=bikepark
DB_HOST=localhost
DB_PORT=5432
DB_NAME=bikepark


export GO111MODULE=on
export GOPRIVATE=gitlab.com/thebikepark/bikepark

################################################################################
# LINT
################################################################################

.PHONY: lint-deps
lint-deps: ## get linter for testing
	GO111MODULE=on go get github.com/golangci/golangci-lint/cmd/golangci-lint@latest

.PHONY:
lint-ci: ## get linter for testing
	curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(go env GOPATH)/bin v1.43.0
	$(go env GOPATH)/bin/golangci-lint run --skip-dirs api/

.PHONY: lint
lint:
	golangci-lint run --skip-dirs api/

################################################################################
# BUILD
################################################################################

.PHONY: build-deps
build-deps:
	go mod tidy

.PHONY: build
build: build-deps ## build the base bikepark application
	GOOS=$(GOOS) GOARCH=$(GOARCH) $(GOENVS) go build -v -ldflags \
		"-X main.Version=$(VERSION) -X main.Build=$(BUILD) $(LDFLAGS)" \
		-tags $(TAGS) \
		$(BUILDFLAGS) -o $(PRODUCT) $(MAIN_PATH)

.PHONY:vendor
vendor:
	go mod vendor

################################################################################
# DOCKER
################################################################################
.PHONY: docker-network
docker-network: ## spin up the local bikepark docker network so that all dockerized gym components can communicate
	if [ -z $$(docker network ls -q --filter 'name=$(DOCKERNET)') ]; then\
		docker network create $(DOCKERNET);\
	fi

.PHONY: docker-bikepark
docker-bikepark: vendor docker-network
	docker-compose --project-directory . \
	-f deployments/docker-compose/docker-compose.yml \
	up \
	-d \
	--force-recreate \
	--build \
	--remove-orphans \
	bikepark

.PHONY: docker-up
docker-up: vendor docker-network
	docker-compose --project-directory . \
	-f deployments/docker-compose/docker-compose.yml \
	up \
	-d \
	--force-recreate \
	--build \
	--remove-orphans

.PHONY: docker-db
docker-db:
	docker-compose --project-directory . \
	-f deployments/docker-compose/docker-compose.yml \
	up \
	-d \
	--build \
	--remove-orphans \
	bikepark.db

.PHONY: docker-logs
docker-logs:
	docker-compose  --project-directory . \
	-f deployments/docker-compose/docker-compose.yml \
	logs

.PHONY: docker-config
docker-config:
	docker-compose --project-directory . \
	-f deployments/docker-compose/docker-compose.yml \
	config

.PHONY: migrate-up
migrate-up:
	migrate -verbose \
	-path $(MIGRATIONS_PATH) \
	-database mysql://$(DB_USER):$(DB_PASS)@tcp\($(DB_HOST):$(DB_PORT)\)/$(DB_NAME) \
	up

.PHONY: migrate-down
migrate-down:
	migrate -verbose \
	-path $(MIGRATIONS_PATH) \
	-database postgres://$(DB_USER):$(DB_PASS)@tcp\($(DB_HOST):$(DB_PORT)\)/$(DB_NAME) \
	down

################################################################################
# TEST
################################################################################
.PHONY: test
test: ## run unit tests and code coverage
	go vet ./...
	go test ./... -v -cover

################################################################################
# PROTO
################################################################################
.PHONY:proto-deps
proto-deps:
	go get google.golang.org/protobuf/proto
	go get github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
	go get github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger
	go get google.golang.org/protobuf/ptypes/empty
	go get github.com/gogo/protobuf/protoc-gen-gogo

.PHONY:proto
proto:
	protoc -I$(PROTOSRC)/include \
		-I$(PROTOSRC)/bikepark \
		--go_out=./pkg/grpc/v1 --go_opt=paths=source_relative\
		--go-grpc_out=./pkg/grpc/v1 --go-grpc_opt=paths=source_relative\
		--grpc-gateway_out=logtostderr=true,paths=source_relative:./pkg/grpc/v1 \
		--openapiv2_out=logtostderr=true:docs \
		$(PROTO_PATH)

