#!/bin/bash

#VARIABLES
PRODUCT="thebikepark/bikepark"
DOCKERREPO="registry.gitlab.com"
IMAGETAG="$PRODUCT:latest"

#CHECK
echo "Building image $IMAGETAG"
docker -v

# docker build -t registry.gitlab.com/thebikepark/bikepark .
# BUILD IMAGE
docker build -t $CI_REGISTRY/$IMAGETAG -f build/docker/Dockerfile .

# LOGIN TO ARTIFACTORY
docker login -u $BIKEPARK_REGISTRY_USER -p $BIKEPARK_REGISTRY_PASSWORD $CI_REGISTRY
#docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

# docker push registry.gitlab.com/thebikepark/bikepark
# PUSH IMAGE TO ARTIFACTORY
docker push $CI_REGISTRY/$IMAGETAG