#!/bin/bash

set -e
set -x

# create a script from a template
migrate -verbose -path ${MIGRATIONS_PATH} \
         -database postgres://${POSTGRES_USER}:${POSTGRES_PASSWORD}@:5432/${POSTGRES_DB}?host=/var/run/postgresql \
         up
